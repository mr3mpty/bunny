package com.empty.bunny;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.empty.bunny.handlers.Content;
import com.empty.bunny.handlers.GameStateManager;
import com.empty.bunny.handlers.MyInput;
import com.empty.bunny.handlers.MyInputProcessor;

public class Game extends ApplicationAdapter {

    public static final String TITLE = "Block Bunny";
    public static final int V_WIDTH = 320;
    public static final int V_HEIGHT = 240;
    public static final int scale = 2;

    public static final float STEP = 1 / 60f;
    private float accum;

    private SpriteBatch sb;
    private OrthographicCamera cam;
    private OrthographicCamera hudCam;

    private GameStateManager gsm;

    public static Content res;

	@Override
	public void create () {
        clearScene();

        Gdx.input.setInputProcessor(new MyInputProcessor());

        res = new Content();
        res.loadTexture("images/bunny.png", "bunny");
        res.loadTexture("images/crystal.png", "crystal");
        res.loadTexture("images/hud.png", "hud");
        sb = new SpriteBatch();

        cam = new OrthographicCamera();
        cam.setToOrtho(false, V_WIDTH, V_HEIGHT);
        hudCam = new OrthographicCamera();
        hudCam.setToOrtho(false, V_WIDTH, V_HEIGHT);

	    gsm = new GameStateManager(this);
    }

	@Override
	public void render () {
        accum += Gdx.graphics.getDeltaTime();
        while(accum >= STEP) {
            clearScene();

            accum -= STEP;
            gsm.update(STEP);
            gsm.render();
            MyInput.update();
        }
    }

    @Override
    public void dispose() {

    }

    public SpriteBatch getSpriteBatch() { return sb; }
    public OrthographicCamera getCamera() { return cam; }
    public OrthographicCamera getHudCam() { return hudCam; }

    private void clearScene(){
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
    }
}
