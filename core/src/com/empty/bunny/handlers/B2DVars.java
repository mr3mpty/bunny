package com.empty.bunny.handlers;

/**
 * Created by mr3mpty on 15.07.2014.
 */
public class B2DVars {

    //pixels per meter ratio
    public static final float PPM = 100;

    //category bits
    public static final short BIT_PLAYER = 2;
    public static final short BIT_RED = 4;
    public static final short BIT_GREEN = 8;
    public static final short BIT_BLUE = 16;
    public static final short BIT_CRYSTAL = 32;


    //above is bit flag so 7 will use 3 categories
}
