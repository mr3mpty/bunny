package com.empty.bunny.handlers;

import com.empty.bunny.Game;
import com.empty.bunny.states.GameState;
import com.empty.bunny.states.Play;

import java.util.Stack;

/**
 * Created by mr3mpty on 15.07.2014.
 */
public class GameStateManager {

    private Game game;
    private Stack<GameState> gameStates;
    public static final int PLAY = 9999;

    public GameStateManager(Game game) {
        this.game = game;
        this.gameStates = new Stack<GameState>();
        pushState(PLAY);
    }

    public Game game() { return game; }

    public void update(float dt) {
        gameStates.peek().update(dt);
    }

    public void render() {
        gameStates.peek().render();
    }

    private GameState getState(int state) {
        if(state == PLAY) return new Play(this);
        return null;
    }

    public void setState(int state) {
        popState();
        pushState(state);
    }

    public void pushState(int state) {
        gameStates.push(getState(state));
    }

    public void popState() {
        GameState g = gameStates.pop();
        g.dispose();
    }


}
