package com.empty.bunny.handlers;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;

/**
 * Created by mr3mpty on 15.07.2014.
 */
public class MyInputProcessor extends InputAdapter {

    @Override
    public boolean keyDown(int k) {
        if(k == Input.Keys.Z) {
            MyInput.setKey(MyInput.BUTTON1, true);
        }

        if(k == Input.Keys.X) {
            MyInput.setKey(MyInput.BUTTON2, true);
        }
        return true;
    }

    @Override
    public boolean keyUp(int k) {
        if(k == Input.Keys.Z) {
            MyInput.setKey(MyInput.BUTTON1, false);
        }

        if(k == Input.Keys.X) {
            MyInput.setKey(MyInput.BUTTON2, false);
        }
        return true;
    }
}
