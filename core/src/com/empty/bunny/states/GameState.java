package com.empty.bunny.states;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.empty.bunny.Game;
import com.empty.bunny.handlers.GameStateManager;

/**
 * Created by mr3mpty on 15.07.2014.
 */
public abstract class GameState {

    protected GameStateManager gsm;
    protected Game game;
    protected SpriteBatch sb;
    protected OrthographicCamera cam;
    protected OrthographicCamera hudCam;

    public GameState(GameStateManager gsm) {
        this.gsm = gsm;
        this.game = gsm.game();
        this.cam = game.getCamera();
        this.hudCam = game.getHudCam();
        this.sb = game.getSpriteBatch();
    }

    public abstract void handleInput();
    public abstract void update(float dt);
    public abstract void render();
    public abstract void dispose();

}
