package com.empty.bunny.states;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.empty.bunny.Game;
import com.empty.bunny.entities.Crystal;
import com.empty.bunny.entities.HUD;
import com.empty.bunny.entities.Player;
import com.empty.bunny.handlers.B2DVars;
import com.empty.bunny.handlers.GameStateManager;
import com.empty.bunny.handlers.MyContactListener;
import com.empty.bunny.handlers.MyInput;

import static com.empty.bunny.handlers.B2DVars.BIT_BLUE;
import static com.empty.bunny.handlers.B2DVars.BIT_CRYSTAL;
import static com.empty.bunny.handlers.B2DVars.BIT_GREEN;
import static com.empty.bunny.handlers.B2DVars.BIT_PLAYER;
import static com.empty.bunny.handlers.B2DVars.BIT_RED;
import static com.empty.bunny.handlers.B2DVars.PPM;

/**
 * Created by mr3mpty on 15.07.2014.
 */
public class Play extends GameState {

    private boolean debug = true;

    private World world;
    private Box2DDebugRenderer b2dr;

    private OrthographicCamera b2dCam;
    private MyContactListener cl;

    private TiledMap tiledMap;
    private float tileSize;
    private OrthogonalTiledMapRenderer mapRenderer;

    private Player player;
    private Array<Crystal> crystals;
    private HUD hud;

    public Play(GameStateManager gsm) {
        super(gsm);

        //setup box2d stuff
        world = new World(new Vector2(0, -9.81f), true);
        cl = new MyContactListener();
        world.setContactListener(cl);
        b2dr = new Box2DDebugRenderer();

        createPlayer();
        createTiles();
        createCrystals();

        hud = new HUD(player);

        //box2dcam
        b2dCam = new OrthographicCamera();
        b2dCam.setToOrtho(false, Game.V_WIDTH / PPM, Game.V_HEIGHT / PPM);


    }

    private void createCrystals() {
        crystals = new Array<Crystal>();
        MapLayer layer = tiledMap.getLayers().get("crystals");
        BodyDef bdef = new BodyDef();
        FixtureDef fdef = new FixtureDef();

        for(MapObject mo : layer.getObjects()) {
            bdef.type = BodyDef.BodyType.StaticBody;

            float x = (Float)mo.getProperties().get("x") / PPM;
            float y = (Float)mo.getProperties().get("y") / PPM;

            bdef.position.set(x,y);

            CircleShape cshape = new CircleShape();
            cshape.setRadius( 8 / PPM);
            fdef.shape = cshape;
            fdef.isSensor = true;
            fdef.filter.categoryBits = B2DVars.BIT_CRYSTAL;
            fdef.filter.maskBits = BIT_PLAYER;

            Body body = world.createBody(bdef);
            body.createFixture(fdef).setUserData("crystal");

            Crystal c = new Crystal(body);
            crystals.add(c);

            body.setUserData(c);
            cshape.dispose();
        }
    }

    private void createTiles() {
        ///tiled stuff
        tiledMap = new TmxMapLoader().load("maps/test.tmx");
        mapRenderer = new OrthogonalTiledMapRenderer(tiledMap);

        tileSize = (Integer) tiledMap.getProperties().get("tilewidth");
        TiledMapTileLayer layer;

        layer = (TiledMapTileLayer) tiledMap.getLayers().get("red");
        createLayer(layer, BIT_RED);
        layer = (TiledMapTileLayer) tiledMap.getLayers().get("green");
        createLayer(layer, BIT_GREEN);
        layer = (TiledMapTileLayer) tiledMap.getLayers().get("blue");
        createLayer(layer, BIT_BLUE);
    }

    private void createLayer(TiledMapTileLayer layer, short bits) {

        BodyDef bdef = new BodyDef();
        FixtureDef fdef = new FixtureDef();

        for (int row = 0; row < layer.getHeight(); row++) {
            for (int col = 0; col < layer.getWidth(); col++) {
                //get cell
                TiledMapTileLayer.Cell cell = layer.getCell(col, row);

                if (cell == null) continue;
                if (cell.getTile() == null) continue;

                bdef.type = BodyDef.BodyType.StaticBody;
                bdef.position.set((col + 0.5f) * tileSize / PPM, (row + 0.5f) * tileSize / PPM);

                ChainShape cs = new ChainShape();
                Vector2[] v = new Vector2[3];
                v[0] = new Vector2(-tileSize / 2 / PPM, -tileSize / 2 / PPM);
                v[1] = new Vector2(-tileSize / 2 / PPM, tileSize / 2 / PPM);
                v[2] = new Vector2(tileSize / 2 / PPM, tileSize / 2 / PPM);

                cs.createChain(v);
                fdef.friction = 0;
                fdef.shape = cs;
                fdef.filter.categoryBits = bits;
                fdef.filter.maskBits = BIT_PLAYER;
                fdef.isSensor = false;

                world.createBody(bdef).createFixture(fdef);
                cs.dispose();
            }
        }
    }

    private void createPlayer() {

        BodyDef bdef = new BodyDef();
        FixtureDef fdef = new FixtureDef();
        PolygonShape shape = new PolygonShape();

        //create player
        bdef.position.set(100 / PPM, 200 / PPM);
        bdef.type = BodyDef.BodyType.DynamicBody;
        bdef.linearVelocity.set(1f, 0);
        Body body = world.createBody(bdef);

        shape.setAsBox(13 / PPM, 13 / PPM);
        fdef.shape = shape;
        fdef.filter.categoryBits = B2DVars.BIT_PLAYER;
        fdef.filter.maskBits = BIT_RED | BIT_CRYSTAL;
        body.createFixture(fdef).setUserData("player");

        //foot sensor
        shape.setAsBox(13 / PPM, 2 / PPM, new Vector2(0, -13 / PPM), 0);
        fdef.shape = shape;
        fdef.filter.categoryBits = B2DVars.BIT_PLAYER;
        fdef.filter.maskBits = BIT_RED;
        fdef.isSensor = true;
        body.createFixture(fdef).setUserData("foot");

        //create player rly
        player = new Player(body);
        body.setUserData(player); //circular reference
        shape.dispose();
    }

    @Override
    public void handleInput() {
        //player jump
        if (MyInput.isPressed(MyInput.BUTTON1) && cl.isPlayerIsOnGround()) {
            player.getBody().applyForceToCenter(0, 250, true);
        }

        if(MyInput.isPressed(MyInput.BUTTON2)) {
            switchBlocks();
        }
    }

    private void switchBlocks() {
        Filter filter = player.getBody().getFixtureList().first().getFilterData();
        short bits = filter.maskBits;

        //switch to next color
        //red->green->blue
        if((bits & B2DVars.BIT_RED) != 0) {
            bits &= ~BIT_RED;
            bits |= BIT_GREEN;
        }
        else if((bits & B2DVars.BIT_GREEN) != 0) {
            bits &= ~BIT_GREEN;
            bits |= BIT_BLUE;
        }
        else if((bits & B2DVars.BIT_BLUE) != 0) {
            bits &= ~BIT_BLUE;
            bits |= BIT_RED;
        }

        filter.maskBits = bits;
        player.getBody().getFixtureList().first().setFilterData(filter);

        filter = player.getBody().getFixtureList().get(1).getFilterData();
        bits &= ~BIT_CRYSTAL;
        filter.maskBits = bits;
        player.getBody().getFixtureList().get(1).setFilterData(filter);
    }

    @Override
    public void update(float dt) {
        handleInput();
        world.step(dt, 6, 2);

        //remove crystals
        Array<Body> bodies = cl.getBodiesToRemove();

        for(Body body : bodies) {
            crystals.removeValue((Crystal) body.getUserData(), true);
            world.destroyBody(body);
            player.collectCrystal();
        }

        bodies.clear();
        player.update(dt);

        for(int i = 0; i < crystals.size; i++) {
            crystals.get(i).update(dt);
        }
    }

    @Override
    public void render() {
        mapRenderer.setView(cam);
        mapRenderer.render();

        //cam follow
        cam.position.set(player.getPosition().x * PPM + Game.V_WIDTH / 4, Game.V_HEIGHT / 2, 0);
        cam.update();

        sb.setProjectionMatrix(cam.combined);
        player.render(sb);

        for(int i = 0; i < crystals.size; i++) {
            crystals.get(i).render(sb);
        }

        sb.setProjectionMatrix(hudCam.combined);
        hud.render(sb);

        if(debug) {
            b2dr.render(world, b2dCam.combined);
        }
    }

    @Override
    public void dispose() {

    }
}
